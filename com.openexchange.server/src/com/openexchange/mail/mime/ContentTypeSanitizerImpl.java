/*
 * @copyright Copyright (c) OX Software GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite.  If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

package com.openexchange.mail.mime;

import javax.mail.internet.ContentTypeSanitizer;
import javax.mail.internet.ParseException;
import com.openexchange.exception.OXException;


/**
 * {@link ContentTypeSanitizerImpl} - The Content-Type sanitizer.
 *
 * @author <a href="mailto:thorben.betten@open-xchange.com">Thorben Betten</a>
 * @since v8.0.0
 */
public class ContentTypeSanitizerImpl implements ContentTypeSanitizer {

    /**
     * Initializes a new {@link ContentTypeSanitizerImpl}.
     */
    public ContentTypeSanitizerImpl() {
        super();
    }

    @Override
    public String sanitizeContentType(String type, ParseException cause) throws ParseException {
        try {
            return new ContentType(type).toString(true);
        } catch (OXException e) {
            throw new ParseException(e.getMessage(), cause);
        }
    }

}
